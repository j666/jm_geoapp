
import Map from 'ol/Map.js';
  import View from 'ol/View.js';
  import Tile from 'ol/layer/Tile.js';
  import OSM from 'ol/source/OSM.js';
  import {fromLonLat} from 'ol/proj';

    
    let map: Map;
    let coordView = [0,0];
    let zoom = 2;
    let view: View;
    const washingtonLonLat = [-77.036667, 38.895];
    const washingtonWebMercator = fromLonLat(washingtonLonLat);
    view =  new View({
          projection:'EPSG:3857',
        // center:  washingtonWebMercator,
        // center:  this.coordView,
        center:  [0, 0],

        // center: proj.transform([-0.12755, 51.507222], 'EPSG:4326', 'EPSG:3857'),
        zoom: 8

      })
    map = new Map({
      layers: [
            new Tile({ source: new OSM(), opacity: 1 }),
            ],

      target: document.getElementById('map'),
      view: view 
    });

    // var but_zoom = document.getElementById('but_zoom');

    // but_zoom.addEventListener('click', function() {
    //     const washingtonWebMercator = fromLonLat([-77.036667, 38.895]);
    
    //     // this.map.view.center = washingtonWebMercator;
    //     // this.map.setView(new View)

    // });
    

    // clickZoom(){
        // console.log('test')
    //     // this.zoom = 2;
    //     let latLon = [38.895646, -77.033500]
    //     this.view.setCenter(fromLonLat([-77.036667, 38.895]));
    //     this.view.setZoom(10);
    //     // this.view.centerOn(fromLonLat([-77.036667, 38.895]));
    //     // this.map.setView(this.view);

    // }
export const MAP = map;



// export const HEROES: Hero[] = [
//   { id: 11, name: 'Mr. Nice' },
//   { id: 12, name: 'Narco' },
//   { id: 13, name: 'Bombasto' },
//   { id: 14, name: 'Celeritas' },
//   { id: 15, name: 'Magneta' },
//   { id: 16, name: 'RubberMan' },
//   { id: 17, name: 'Dynama' },
//   { id: 18, name: 'Dr IQ' },
//   { id: 19, name: 'Magma' },
//   { id: 20, name: 'Tornado' }
// ];