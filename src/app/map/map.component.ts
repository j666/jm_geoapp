import { Component, Input, OnInit } from '@angular/core';
import { FormsModule} from '@angular/forms';

import Map from 'ol/Map.js';
  import View from 'ol/View.js';
  import Tile from 'ol/layer/Tile.js';
  import OSM from 'ol/source/OSM.js';
  import proj from 'ol/proj';
  import {fromLonLat} from 'ol/proj';
  import {toLonLat} from 'ol/proj';
import MousePosition from 'ol/control/MousePosition';
import {createStringXY} from 'ol/coordinate.js';
import {defaults as defaultControls} from 'ol/control.js';


import {Image} from 'ol/layer.js';
import ImageWMS from 'ol/source/ImageWMS.js';


import { MAP } from '../mapConst';
import { debug } from 'util';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit {

    test:string = 'bonjour';
    private map = MAP;
    // private map: Map;
    private coordView = [0,0];
    private coordonee = [0, 0];
    private zoom = 2;
    private view: View;

    @Input() latitude:number;
    @Input() longitude :number;

    @Input() coord :string;
    // private coordonee :Number[];


    constructor() { }




 ngOnInit() {

    const washingtonLonLat = [-77.036667, 38.895];
    const washingtonWebMercator = fromLonLat(washingtonLonLat);
    this.view =  new View({
          projection:'EPSG:3857',
        // center:  washingtonWebMercator,
        // center:  this.coordView,
        center:  [0, 0],

        // center: proj.transform([-0.12755, 51.507222], 'EPSG:4326', 'EPSG:3857'),
        zoom: this.zoom

      })
    this.map = new Map({
      layers: [
            new Tile({ source: new OSM(), opacity: 1 }),
            ],

      target: document.getElementById('map'),
      view:this.view,
    //   controls: defaultControls().extend([this.mousePositionControl]), 
    });

    this.map.on('singleclick', function (evt) {
        debugger;
        console.log('clic');
        console.log(toLonLat(this.getCoordinateFromPixel(evt.pixel)));
        var coordClick = toLonLat(this.getCoordinateFromPixel(evt.pixel))
        // document.getElementById("html-coord").innerHTML = 'test';
        var coordClick = [coordClick[0].toFixed(4), coordClick[1].toFixed(4)]
        document.getElementById("html-coord").innerHTML = coordClick;
        // console.log(this.coord)
    })

//     this.map.getViewport().addEventListener('contextmenu', function (evt) {
// evt.preventDefault();


//     let mousePositionControl = new MousePosition({
//         coordinateFormat: createStringXY(4),
//         projection: 'EPSG:4326',
//         // comment the following two lines to have the mouse position
//         // be placed within the map.
//         className: 'custom-mouse-position',
//         target: document.getElementById('mouse-position'),
//         undefinedHTML: '&nbsp;'
//       });

// console.log(this.mousePositionControl)
// console.log(this.mousePosition)
// // console.log(this.mouse)

// // console.log(this.map.getEventCoordinate(evt));
//     })


    // var but_zoom = document.getElementById('but_zoom');

    // but_zoom.addEventListener('click', function() {
    //     const washingtonWebMercator = fromLonLat([-77.036667, 38.895]);
    
        // this.map.view.center = washingtonWebMercator;
        // this.map.setView(new View)

    // });
    }



    clickZoom(){

        // let coord:Number[] = [0,0]
        debugger
        // console.log(Number(this.longitude))

        this.coordonee[0] = Number(this.longitude)
        this.coordonee[1] = Number(this.latitude)
        console.log(this.coordonee)
        // this.zoom = 2;

        this.view.setCenter(fromLonLat(this.coordonee));
        this.view.setZoom(10);
        // this.view.centerOn(fromLonLat([-77.036667, 38.895]));
        // this.map.setView(this.view);

    }
}
