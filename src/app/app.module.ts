import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TitreComponent } from './titre/titre.component';
import { MapComponent } from './map/map.component';
import { RechercheCoordComponent } from './recherche-coord/recherche-coord.component';

@NgModule({
  declarations: [
    AppComponent,
    TitreComponent,
    MapComponent,
    RechercheCoordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
